mkdir -p output/pipeline

$MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.py -s '1-15' \
    -l debug \
    -r input/raw/Readset.txt \
    -d input/raw/Design.txt \
    -o output/pipeline \
    --config $MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.base.ini \
        $MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.guillimin.ini \
        $MUGQIC_PIPELINES_HOME/resources/genomes/config/Homo_sapiens.GRCh38.ini \
        input/chipseq.numpy.bug.ini

