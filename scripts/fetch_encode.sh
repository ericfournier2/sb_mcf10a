# Create directory for ENCODE files.
mkdir -p input/raw

# Download them.
for file in `sed 1d input/MCF10A-ENCODE.txt | cut -f 10`
do 
    wget $file -nc -P input/raw/ &
done

