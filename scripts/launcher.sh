#!/bin/bash

# ARGUMENT TESTING

if [  $# -ne 3 ] 
then 
  echo "Usage : /is1/projects/SB_NGS/Hi-C_pipeline/launcher.sh </FULL/PATH/TO/FILE.SRA> </FULL/PATH/TO/OUTPUT_FOLDER> <RESOLUTION_IN_BASES>"
  echo " "
  echo "Example : /is1/projects/SB_NGS/Hi-C_pipeline/launcher.sh /is1/projects/SB_NGS/mvallee/test.sra /is1/projects/SB_NGS/mvallee/test_out 4000"
  echo " "
  exit 1
fi 

hicPATH=`grep -o Hi-C_pipeline ~/.bashrc | wc -l`
 
#if [  $hicPATH -ne 2 ]
#then
#  echo "Please add to your ~/.bashrc file the following line :"
#  echo "PATH=\$PATH:/is1/projects/SB_NGS/Hi-C_pipeline/circos-0.68/bin/:/is1/projects/SB_NGS/Hi-C_pipeline/HOMER/bin/"
#  echo "and run"
#  echo "source ~/.bashrc"
#  echo "before relaunching the command."
#fi

# Clean variables

if [ ! -f $1 ]
then
  echo "$1 not found, exiting!"
  exit
fi

outputFolder=${2%/}
id=`basename $1 .sra`

# Recap
echo "------------------------------------------------"
echo -n "HIC ANALYSIS starting "
LANG="en_US.utf-8"; date; LANG="fr_CA.UTF-8"
echo "SRA file : "$1
echo "Output folder : "$outputFolder
echo "Resolution : "$3" bases"
echo "------------------------------------------------"
echo " "


# Output folder creation and move there

mkdir $outputFolder > /dev/null 2>&1
#cd $outputFolder

if [ ! -f $outputFolder/${id}_2.fastq.gz ]
then
  echo -n "Step 1 : Convert SRA file in 2 FASTQ files with SRAtoolkit fastq-dump "
  LANG="en_US.utf-8"; date; LANG="fr_CA.UTF-8"
  module load sratoolkit
  /software/sratoolkit/sratoolkit.2.6.2-centos_linux64/bin/fastq-dump --gzip --split-3 -O "$outputFolder" $1
echo " "
fi

reference=/is1/projects/SB_NGS/Hi-C_pipeline/reference/GRCh38/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.bowtie_index

if [ ! -f $outputFolder/${id}_1.sam ]
then
  echo -n "Step 2a : Align first in pair read with Bowtie2 "
  LANG="en_US.utf-8"; date; LANG="fr_CA.UTF-8"
  #module load bowtie2
  #bowtie2 -x $reference -U $outputFolder/${id}_1.fastq.gz -S $outputFolder/${id}_1.sam -p 8
  /software/bowtie2/bowtie2-2.2.3/bin/bowtie2 -x $reference -U $outputFolder/${id}_1.fastq.gz -S $outputFolder/${id}_1.sam -p 8
  #/Hi-C_pipeline/reference/ucsc.hg19
  echo " "
fi

if [ ! -f $outputFolder/${id}_2.sam ]
then
  echo -n "Step 2b : Align second in pair read with Bowtie2 "
  LANG="en_US.utf-8"; date; LANG="fr_CA.UTF-8"
  module load bowtie2
  bowtie2 -x $reference -U $outputFolder/${id}_2.fastq.gz -S $outputFolder/${id}_2.sam -p 8
  #/software/bowtie2/bowtie2-2.2.3/bin/bowtie2 -x $reference -U $outputFolder/${id}_2.fastq.gz -S $outputFolder/${id}_2.sam -p 8
  #/Hi-C_pipeline/reference/ucsc.hg19
  echo " "
fi

if [ ! -f $outputFolder/${id}_HiC-tags/tagInfo.txt ]
then
  echo -n "Step 3 : Make Tag Directory with HOMER "
  LANG="en_US.utf-8"; date; LANG="fr_CA.UTF-8"
  /is1/projects/SB_NGS/Hi-C_pipeline/HOMER_2/bin/makeTagDirectory $outputFolder/${id}_HiC-tags $outputFolder/${id}_1.sam,$outputFolder/${id}_2.sam -tbp 1
  #cp -r ES-HiC-unfiltered ES-HiC-filtered
  #makeTagDirectory ES-HiC-filtered -update -genome hg19 -removePEbg -restrictionSite GATC -both -removeSelfLigation -removeSpikes 10000 5
  #/is1/projects/SB_NGS/Hi-C_pipeline/HOMER/bin/makeTagDirectory ES-HiC-filtered -update -genome hg19 -removePEbg -restrictionSite AAGCTT -both -removeSelfLigation -removeSpikes 10000 5
  echo " "
fi

if [ ! -f $outputFolder/${3}b_significantInteractions.NOCHR.txt ]
then
  echo -n "Step 4 : Analyze significant interactions at ${3} bases with HOMER "
  LANG="en_US.utf-8"; date; LANG="fr_CA.UTF-8"
  /is1/projects/SB_NGS/Hi-C_pipeline/HOMER_2/bin/analyzeHiC $outputFolder/${id}_HiC-tags -res $3 -interactions $outputFolder/${3}b_significantInteractions.NOCHR.txt -nomatrix -cpu 8
  echo " "
fi

if [ ! -f ${id}_Annotation/interactionAnnotation.txt ]
then
  echo -n "Step 5 : Interactions annotation with HOMER "
  LANG="en_US.utf-8"; date; LANG="fr_CA.UTF-8"
  awk 'BEGIN{FS="\t"}{OFS="\t"}{if(NR!=1){print $1,"chr"$2,"chr"$3,$4,$5,$6,$7,"chr"$8,"chr"$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20}else{print $0}}' $outputFolder/${3}b_significantInteractions.NOCHR.txt > $outputFolder/${3}b_significantInteractions.txt
  /is1/projects/SB_NGS/Hi-C_pipeline/HOMER_2/bin/annotateInteractions.pl $outputFolder/${3}b_significantInteractions.txt hg19 $outputFolder/${id}_Annotation/
  echo " "
fi

echo "------------------------------------------------"
echo -n "HIC ANALYSIS ending "
LANG="en_US.utf-8"; date; LANG="fr_CA.UTF-8"
echo "------------------------------------------------"
echo " "




