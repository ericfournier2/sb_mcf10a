# Full study (MCF7 and MCF-10A): GSE66733 - https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE66733
# Sample of interest: GSM1631184 - https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1631184

wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX950/SRX950724/SRR1909069/SRR1909069.sra -nc -P input/raw/