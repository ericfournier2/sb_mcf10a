# Add executables to PATH.
export PATH=$PATH:/is1/projects/SB_NGS/Hi-C_pipeline/circos-0.68/bin/:/is1/projects/SB_NGS/Hi-C_pipeline/HOMER/bin/

module load sratoolkit

# Launch pipeline.
scripts/launcher.sh input/SRR1909069.sra output/MCF10A_4K 4000
